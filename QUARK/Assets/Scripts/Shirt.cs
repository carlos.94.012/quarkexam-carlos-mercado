using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Shirt : Wear
{
    Neck neckType;
    Arm armType;


    public float Price { get => price; }
    public int Stock { get => stock; }
    public Quality WearQuality { get => wearQuality; }
    public Neck NeckType { get => neckType; }
    public Arm ArmType { get => armType; }


    public Shirt(int sStock, Quality qQuality, Arm aArmType, Neck nNeckType)
    {
        stock = sStock;
        wearQuality = qQuality;
        armType = aArmType;
        neckType = nNeckType;
    }

    public Shirt(Quality qQuality, Arm aArmType, Neck nNeckType, float pPrice)
    {
        price = pPrice;
        wearQuality = qQuality;
        armType = aArmType;
        neckType = nNeckType;
    }

    public override float Cotizar(int amount)
    {
        
        float total = price * amount;

        float armDiscount = 0;
        if (armType == Arm.SHORT) armDiscount = -total * 0.1f;

        float neckDiscount = 0;
        if (neckType == Neck.MAO) neckDiscount = total * 0.03f;

        float qualityDiscount = 0;
        if (wearQuality == Quality.PREMIUM) qualityDiscount = total * 0.3f;

        total += armDiscount + neckDiscount + qualityDiscount;
        return total;
    }
}
