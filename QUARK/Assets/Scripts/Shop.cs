using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop
{
    string name;
    string adress;
    List<Wear> stockWear;


    public Shop(string nName, string aAdress, List<Wear> sStockWear)
    {
        name = nName;
        adress = aAdress;
        stockWear = sStockWear;
    }

    

    public int CheckStock(Shirt newShirt)
    {
        int isStock = 0;

        

        foreach (Wear item in stockWear)
        {
            if (item.GetType().Name.ToUpper() == "SHIRT")
            {
                Shirt s = (Shirt)item;
                if (s.NeckType == newShirt.NeckType && s.ArmType == newShirt.ArmType && s.WearQuality == newShirt.WearQuality)
                {
                    isStock = s.Stock;
                }
            }
        }
        return isStock;

    }

    public int CheckStock(Pants newPants)
    {

        int isStock = 0;

        foreach (Wear item in stockWear)
        {
            if (item.GetType().Name.ToUpper() == "PANTS")
            {
                Pants p = (Pants)item;
                if (p.PantType == newPants.PantType && p.WearQuality == newPants.WearQuality)
                {
                    isStock = p.Stock;
                }
            }
        }
        return isStock;


    }

}
