using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Pants : Wear
{
    LegFit pantType;
    public float Price { get => price; }
    public int Stock { get => stock; }
    public Quality WearQuality { get => wearQuality; }

    public LegFit PantType { get => pantType; }





    public Pants(int sStock, Quality qQuality, LegFit pPantType)
    {
        stock = sStock;
        wearQuality = qQuality;
        pantType = pPantType;
    }

    public Pants(Quality qQuality, LegFit pPantType, float pPrice)
    {
        price = pPrice;
        wearQuality = qQuality;
        pantType = pPantType;
    }


    public override float Cotizar(int amount)
    {
        float total = price * amount;

        float legFitDiscount = 0;
        if (pantType == LegFit.FIT) legFitDiscount = -total * 0.12f;
       
        float qualityDiscount = 0;
        if (wearQuality == Quality.PREMIUM) qualityDiscount = total * 0.3f;

        total += legFitDiscount + qualityDiscount;
        return total;
    }
}
