using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIProgram : MonoBehaviour
{

    [SerializeField] DropdownsWear dropdownsValues;
    [SerializeField] TMP_Text txtTotal;

    [SerializeField] Color colorQuoteSucess;
    [SerializeField] Color colorQuoteError;
    [SerializeField] string msgError;
    [SerializeField] UIHistorial guisHistorical;

    [SerializeField] DatosInfo datosShop;
    public void QuitApp()
    {
        Debug.Log("Cerrar App");
        Application.Quit();
    }

    public void SeeSales()
    {
        //guisHistorical.gameObject.SetActive(true);
        Instantiate(guisHistorical);
    }

    public void ButtonCotizar()
    {
        
        float priceUnit = dropdownsValues.priceUnityIngresed;
        int quantity = dropdownsValues.quantityIngresed;

        Quote newQuote = null;
        Quality qa = dropdownsValues.qualitySelected;

        if (dropdownsValues.wearSelected.ToUpper() == "SHIRT")
        {
            Arm arm = dropdownsValues.armSelected;
            Neck neck = dropdownsValues.neckSelected;
            
            Shirt newShirt = new Shirt(qa, arm, neck, priceUnit);

            if(datosShop.MyShop.CheckStock(newShirt) >= quantity)
            {
                string descriptionWear = "Shirt, " + arm.ToString() + ", " + neck.ToString();                
                newQuote = new Quote(datosShop.Seller.Quotes.Count + 1, datosShop.Seller.Id, descriptionWear, quantity, newShirt.Cotizar(quantity));
                txtTotal.text = "U$D " + newQuote.Total;
                txtTotal.color = colorQuoteSucess;
            }
            else
            {
                txtTotal.text = msgError.ToUpper() ;
                txtTotal.color = colorQuoteError;
            }
        }
        else
        {
            LegFit leg = dropdownsValues.legFitSelected;

            Pants newPants = new Pants(qa, leg, priceUnit);

            if (datosShop.MyShop.CheckStock(newPants) >= quantity)
            {
                string descriptionWear = "Pants, " + leg.ToString();
                newQuote = new Quote(datosShop.Seller.Quotes.Count + 1, datosShop.Seller.Id, descriptionWear, quantity, newPants.Cotizar(quantity));
                txtTotal.text = "U$D " + newQuote.Total;
                txtTotal.color = colorQuoteSucess;
            }
            else
            {
                txtTotal.text = msgError.ToUpper();
                txtTotal.color = colorQuoteError;
            }
        }

        if (newQuote != null)
        {
            datosShop.Seller.AddQuote(newQuote);
        }
        
    }
    

    
}
