using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHistorial : MonoBehaviour
{
    [SerializeField] CotizationData quoteData;
    [SerializeField] GameObject parentQuotesData;
     DatosInfo datosShop;
    void Start()
    {
        datosShop = FindObjectOfType<DatosInfo>();
        GetComponent<Canvas>().worldCamera = Camera.main;
        ChargeQuote();
    }

    void ChargeQuote()
    {
        foreach (Quote item in datosShop.Seller.Quotes)
        {
            CotizationData cd = Instantiate(quoteData, parentQuotesData.transform);
            cd.ChargeData(item);
        }       
    }
    
    public void CloseHistorial()
    {
        Destroy(gameObject);
    }
   
    /*
    private void OnDisable()
    {
        for (int i = 1; i < parentQuotesData.transform.childCount; i++)
        {
            Destroy(parentQuotesData.transform.GetChild(i).gameObject);
        }
    }*/
}
