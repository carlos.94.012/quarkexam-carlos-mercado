using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vendedor : IPrint
{
    string name;
    string surname;
    int id;

    List<Quote> quotes = new List<Quote>();

    public Vendedor(string nName, string sSurname, int key)
    {
        name = nName;
        surname = sSurname;
        id = key;
    }



    public int Id { get => id;}
    public List<Quote> Quotes { get => quotes;}

    public void AddQuote(Quote q)
    {
        quotes.Add(q);
    }



    public string CompleteName()
    {
        return name + " " + surname;
    }

    public void Print()
    {
        //Imprimir
    }
}
