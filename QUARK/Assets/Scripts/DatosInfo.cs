using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DatosInfo : MonoBehaviour
{
    Vendedor seller;
    Shop myShop;
    public  Vendedor Seller { get => seller;  }
    public  Shop MyShop { get => myShop; }

    List<Wear> initStock;

    //PODEMOS SUSTITUIRLO POR SCRIPTABLES
    [SerializeField] string shopName;
    [SerializeField] string shopAdress;
    [SerializeField] string vendorName;
    [SerializeField] string vendorSurname;
    [SerializeField] int vendorId;

    [Space(20)]

    [SerializeField] TMP_Text txtShopName;
    [SerializeField] TMP_Text txtShopAdress;
    [SerializeField] TMP_Text txtVendorName;
    [SerializeField] TMP_Text txtVendorId;


    
    private void Start()
    {
        initStock = new List<Wear>();

        initStock.Add(new Shirt(100, Quality.STANDARD, Arm.SHORT, Neck.MAO));
        initStock.Add(new Shirt(100, Quality.PREMIUM, Arm.SHORT, Neck.MAO));

        initStock.Add(new Shirt(150, Quality.STANDARD, Arm.SHORT, Neck.COMMON));
        initStock.Add(new Shirt(150, Quality.PREMIUM, Arm.SHORT, Neck.COMMON));

        initStock.Add(new Shirt(75, Quality.STANDARD, Arm.LONG, Neck.MAO));
        initStock.Add(new Shirt(75, Quality.PREMIUM, Arm.LONG, Neck.MAO));

        initStock.Add(new Shirt(175, Quality.STANDARD, Arm.LONG, Neck.COMMON));
        initStock.Add(new Shirt(175, Quality.PREMIUM, Arm.LONG, Neck.COMMON));


        initStock.Add(new Pants(750, Quality.STANDARD, LegFit.FIT));
        initStock.Add(new Pants(750, Quality.PREMIUM, LegFit.FIT));

        initStock.Add(new Pants(250, Quality.STANDARD, LegFit.COMMON));
        initStock.Add(new Pants(250, Quality.PREMIUM, LegFit.COMMON));

        
        myShop = new Shop(shopName, shopAdress, initStock);
        seller = new Vendedor(vendorName, vendorSurname, vendorId );

        SetTextInit();

    }

    void SetTextInit()
    {
        txtShopName.text = shopName;
        txtShopAdress.text = shopAdress;

        txtVendorName.text = seller.CompleteName();
        txtVendorId.text = vendorId.ToString();
    }
}
