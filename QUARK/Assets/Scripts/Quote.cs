using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quote :IPrint
{

    int id;
    DateTime date;
    int idVendor;
    string description;
    int amount;
    float total;


    public Quote(int key, int sellerId, string wearDescription, int quantity, float moneyTotal)
    {
        id = key;
        idVendor = sellerId;
        description = wearDescription;
        amount = quantity;
        total = moneyTotal;
        date = System.DateTime.Now;
    }

    public float Total { get => total;  }
    public int Id { get => id;  }
    public DateTime Date { get => date; }
    public int IdVendor { get => idVendor;  }
    public string Description { get => description;  }
    public int Amount { get => amount;  }

    public void Print()
    {
        //Imprimir
    }
}
