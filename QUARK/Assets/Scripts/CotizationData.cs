using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CotizationData : MonoBehaviour
{
    [SerializeField] TMP_Text txtId;
    [SerializeField] TMP_Text txtDate;
    [SerializeField] TMP_Text txtSeller;
    [SerializeField] TMP_Text txtDescription;
    [SerializeField] TMP_Text txtAmount;
    [SerializeField] TMP_Text txtTotal;

    
    public void ChargeData(Quote q)
    {
        txtId.text = q.Id.ToString();
        txtDate.text = q.Date.ToString();
        txtSeller.text = q.IdVendor.ToString();
        txtDescription.text = q.Description.ToString().ToUpper();
        txtAmount.text = q.Amount.ToString();
        txtTotal.text = q.Total.ToString();
    }
}
