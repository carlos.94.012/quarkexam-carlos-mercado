using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Wear 
{
    protected float price;
    protected int stock;
    protected Quality wearQuality;

    

    public abstract float Cotizar(int amount);
    
}
