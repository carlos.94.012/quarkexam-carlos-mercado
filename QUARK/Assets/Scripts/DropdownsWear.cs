using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DropdownsWear : MonoBehaviour
{
    [SerializeField] TMP_Dropdown drpClothe;
    [SerializeField] TMP_Dropdown drpNeck;
    [SerializeField] TMP_Dropdown drpArm;
    [SerializeField] TMP_Dropdown drpLegFit;
    [SerializeField] TMP_Dropdown drpQuality;

    [SerializeField] TMP_InputField inpQuantity;
    [SerializeField] TMP_InputField inpPriceUnit;

    [SerializeField] Button btnCotize;

    [SerializeField] GameObject shirtOptions;
    [SerializeField] GameObject pantsOptions;

    public string wearSelected { get => drpClothe.options[drpClothe.value].text; }
    public Neck neckSelected { get =>(Neck)System.Enum.Parse(typeof(Neck),  drpNeck.options[drpNeck.value].text); }
    public Arm armSelected { get =>(Arm)System.Enum.Parse(typeof(Arm), drpArm.options[drpArm.value].text); }
    public LegFit legFitSelected { get =>(LegFit)System.Enum.Parse(typeof(LegFit), drpLegFit.options[drpLegFit.value].text); }
    public Quality qualitySelected { get =>(Quality)System.Enum.Parse(typeof(Quality), drpQuality.options[drpQuality.value].text); }

    public int quantityIngresed { get => int.Parse(inpQuantity.text); }
    public float priceUnityIngresed { get => float.Parse(inpPriceUnit.text); }

    private void Start()
    {
        ChangeInputs();

        drpClothe.ClearOptions();
        drpNeck.ClearOptions();
        drpArm.ClearOptions();
        drpLegFit.ClearOptions();
        drpQuality.ClearOptions();

        SetDropdownsValues(drpClothe,  new string[] {"SHIRT", "PANTS"});
        SetDropdownsValues(drpNeck, System.Enum.GetValues(typeof(Neck)));
        SetDropdownsValues(drpArm, System.Enum.GetValues(typeof(Arm))); 
        SetDropdownsValues(drpLegFit, System.Enum.GetValues(typeof(LegFit)));
        SetDropdownsValues(drpQuality, System.Enum.GetValues(typeof(Quality)));
    }

    public void WearDropdown()
    {
        shirtOptions.SetActive(false);
        pantsOptions.SetActive(false);

        switch (drpClothe.options[drpClothe.value].text)
        {
            case "SHIRT":
                shirtOptions.SetActive(true);
                break;
            case "PANTS":
                pantsOptions.SetActive(true);
                break;
        }
    }



    void SetDropdownsValues(TMP_Dropdown drp, System.Array values)
    {
        List<string> options = new List<string>();

        foreach (var item in values)
        {
            options.Add(item.ToString());
        }
        drp.AddOptions(options);
    }

    void SetDropdownsValues(TMP_Dropdown drp, string[] values)
    {
        List<string> options = new List<string>();

        foreach (var item in values)
        {
            options.Add(item.ToString());
        }
        drp.AddOptions(options);
    }

    public void ChangeInputs()
    {
        if (inpQuantity.text != "" && inpPriceUnit.text != "")
        {
            btnCotize.interactable = true;
        }
        else
        {
            btnCotize.interactable = false;
        }
    }
}
